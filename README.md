# Deep Learning Seminar

## Install Requirements

0. AWS EC2 settings

AMI: Deep Learning AMI GPU Tensorflow 2.14
Instance type: g4dn.xlarge
Storage: 30 GB

1. Install `python3.10` and venv:

```bash
sudo apt update && sudo apt upgrade -y
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa 
sudo apt install python3.11
sudo apt-get install python3.11-venv
```

2. Clone the repository and create a local virtual environment:

```bash
git clone git@gitlab.com:bryskulov/deep_learning_template.git
cd image_classifier
python3 -m venv venv
source venv/bin/activate
```

3. Load required environment variables.

```bash
export $(cat .env | xargs)
```

4. Install all the dependencies:

```bash
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
```

5. Allow permissions

Give S3 access to your EC2 by modifying IAM role to `s3_full_access_from_ec2`.


## Track Experiments
Launch mlflow tracking URL:

```bash
cd image_classifier/
mlflow server --backend-store-uri=sqlite:///mlflow.db
```

All results from simulations has to be tracked in mlflow. To analyze the results one can use
mlflow web UI. Mlflow allows to analyze the results directly from web or download them as `csv` file.

## Run Training

The training script with the configurations are stored as shell script in `run.sh` file. One can make adjustments to training parameters in the same script and run it:

```bash
python train.py
```

### Launch tensorboard to Track Training

Launch tensorboard
```bash
tensorboard --logdir logs_tensorboard
```

Forward the port and open it in browser, usually in address: `127.0.0.1:6006`.
