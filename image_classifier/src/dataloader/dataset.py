import pathlib
from typing import Dict, List, Tuple

import tensorflow as tf
from tensorflow.keras.utils import image_dataset_from_directory


def download_dataset():
    dataset_url = (
        "https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz"
    )
    data_dir = tf.keras.utils.get_file("flower_photos.tar", origin=dataset_url, extract=True)
    data_dir = pathlib.Path(data_dir).with_suffix("")
    return data_dir


class FlowerDataset:
    """
    Dataset class representing the structure of the image dataset.
    """

    def __init__(
        self,
        image_dir: str,
        batch_size: int = 32,
        image_size: Tuple = (224, 224),
        shuffle: bool = True,
        seed: int = 42,
        val_split: float = 0.2,
    ) -> None:
        """
        Dataset class representing the structure of the image dataset.

        Args:
            image_dir: The root image directory
            batch_size: Size of the batches of data
            shuffle: Whether to shuffle the data
        """
        self.image_dir = image_dir
        self.batch_size = batch_size
        self.image_size = image_size
        self.shuffle = shuffle
        self.seed = seed
        self.val_split = val_split
        self.train = None
        self.val = None

    def load(self) -> None:
        """
        Loads the data from the directory and stores them as attributes.
        """
        self.train = image_dataset_from_directory(
            self.image_dir,
            validation_split=self.val_split,
            subset="training",
            seed=self.seed,
            image_size=self.image_size,
            batch_size=self.batch_size,
            shuffle=self.shuffle,
        )
        self.val = image_dataset_from_directory(
            self.image_dir,
            validation_split=self.val_split,
            subset="validation",
            seed=self.seed,
            image_size=self.image_size,
            batch_size=self.batch_size,
            shuffle=self.shuffle,
        )
        self.class_names = self.train.class_names
        self.train_file_paths = self.train.file_paths
        self.val_file_paths = self.val.file_paths
        self._configure_dataset()

    def _configure_dataset(self):
        AUTOTUNE = tf.data.AUTOTUNE
        self.train = self.train.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
        self.val = self.val.cache().prefetch(buffer_size=AUTOTUNE)

    def get_sample_size(self) -> Dict[str, Tuple[int, int]]:
        """
        Calculates the size of the dataset.

        Returns:
            Number of train and val samples
        """
        dataset_size = {"train": len(self.train_file_paths), "val": len(self.val_file_paths)}
        return dataset_size
