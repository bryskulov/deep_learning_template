import datetime
import os
import time
from typing import Dict, List, Optional, Union

import mlflow
import mlflow.keras
import numpy as np
import tensorflow as tf
from keras.callbacks import Callback
from src.utils.utils import load_model
from tensorflow import keras


class TimingCallback(Callback):
    """The callback which records the epoch training times."""

    def on_train_begin(self, logs={}):
        self.epoch_times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        self.epoch_times.append(round(time.time() - self.epoch_time_start, 2))


class FlowerClassifier:
    def __init__(
        self,
        model: keras.Model,
        loss: Union[keras.losses.Loss, str] = tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=True
        ),
        optimizer: Union[keras.optimizers.Optimizer, str] = "Adam",
        tensorboard_log_dir: str = "tensorboard_logs/",
        csv_log_dir: str = "csv_logs/",
        saved_models_path: str = "saved_models/",
        run_id: Union[None, str] = None,
    ):
        self.model = model
        self.model_name = model.name
        self.loss = loss
        self.optimizer = optimizer
        self.tensorboard_log_dir = tensorboard_log_dir
        self.csv_log_dir = csv_log_dir
        self.saved_models_path = saved_models_path
        self.callbacks: Optional[List] = None
        self.run_id = run_id
        self._create_dirs()

    def _create_dirs(self):
        if not os.path.exists(self.tensorboard_log_dir):
            os.mkdir(self.tensorboard_log_dir)
        if not os.path.exists(self.csv_log_dir):
            os.mkdir(self.csv_log_dir)
        if not os.path.exists(self.saved_models_path):
            os.mkdir(self.saved_models_path)

    def _create_callbacks(self, saved_model_name):
        """Creates keras callbacks and stores them as attribute."""
        log_dir = (
            self.tensorboard_log_dir
            + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            + "-"
            + self.run_id
        )
        tensorboard_callback = keras.callbacks.TensorBoard(
            log_dir=log_dir, histogram_freq=1, profile_batch=(20, 25)
        )
        model_checkpoint_callback = keras.callbacks.ModelCheckpoint(
            filepath=f"{self.saved_models_path}{saved_model_name}.h5",
            save_best_only=True,
            monitor="val_loss",
            mode="min",
        )
        early_stopping_callback = keras.callbacks.EarlyStopping(
            monitor="val_loss",
            patience=5,
            restore_best_weights=True,
            start_from_epoch=0,
        )

        csv_log_dir = (
            self.csv_log_dir
            + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            + "-"
            + self.run_id
            + ".csv"
        )
        csv_logger_callback = keras.callbacks.CSVLogger(csv_log_dir, separator=",", append=True)

        callbacks = [
            model_checkpoint_callback,
            tensorboard_callback,
            early_stopping_callback,
            csv_logger_callback,
        ]
        self.callbacks = callbacks

    def _compile(self):
        """Compiles the model with predefined metrics."""
        self.model.compile(loss=self.loss, optimizer=self.optimizer, metrics=["accuracy"])

    def _count_params(self):
        """Counts total parameters."""
        params = self.model.count_params()

        return params

    def fit(
        self,
        data,
        epochs: int,
        mlflow_tags: Dict = None,
        log_models: bool = False,
        saved_model_name: str = "test_model",
    ) -> tf.keras.callbacks.History:
        self._compile()
        params_counts = self._count_params()

        mlflow.tensorflow.autolog(disable=False, log_models=log_models)
        with mlflow.start_run() as run:
            # mlflow logging
            if mlflow_tags:
                mlflow.set_tags(mlflow_tags)
            mlflow.log_param("Parameters count", params_counts)

            # create callbacks
            self.run_id = run.info.run_id
            self._create_callbacks(saved_model_name=saved_model_name)
            time_callback = TimingCallback()

            # train the model
            history = self.model.fit(
                data.train,
                epochs=epochs,
                validation_data=data.val,
                callbacks=self.callbacks + [time_callback],
            )

            # evaluate on test set
            start_time = time.time()
            model_path = self.saved_models_path + saved_model_name + ".h5"
            saved_model = load_model(model_path)
            model_load_time = round(time.time() - start_time, 10)

            start_time = time.time()
            test_metrics = saved_model.evaluate(data.val)
            inference_time = round(time.time() - start_time, 10)

            # mlflow logging
            mlflow.log_metric("val_loss", test_metrics[0])
            mlflow.log_metric("accuracy", test_metrics[1])
            mlflow.log_param("10 epoch_times", time_callback.epoch_times[:10])
            mlflow.log_metric("train_time", sum(time_callback.epoch_times))
            mlflow.log_metric("model_load_time", model_load_time)
            mlflow.log_metric("inference_time", inference_time)

            mlflow.log_artifact(model_path, artifact_path="model")

            print(f"Test loss: {test_metrics[0]:.3f}")

        return history
