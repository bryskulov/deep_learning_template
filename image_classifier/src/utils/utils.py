from tensorflow import keras


def load_model(model_path: str) -> keras.Model:
    """
    Loads the keras model from the filepath.

    Args:
        model_path: The path to the model.

    Return:
        Return the loaded keras model.
    """
    model = keras.models.load_model(model_path)
    return model
