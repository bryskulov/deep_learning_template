import mlflow
from mlflow.tracking import MlflowClient
import tensorflow as tf
from src.dataloader.dataset import FlowerDataset, download_dataset
from src.model.cnn import basic_cnn
from src.model.flower_classifer import FlowerClassifier

batch_size = 32
image_size = (256, 256)
random_seed = 42
epochs = 5

# mlflow.set_tracking_uri("http://127.0.0.1:5000")
TRACKING_SERVER_HOST = "172.31.18.159" #Private IPv4 addresses
mlflow.set_tracking_uri(f"http://{TRACKING_SERVER_HOST}:5000") 
print(f"Tracking Server URI: '{mlflow.get_tracking_uri()}'")
mlflow.set_experiment("flower-classifier")


def main():
    # Preparing dataset
    data_path = download_dataset()
    data_flower = FlowerDataset(
        image_dir=data_path,
        batch_size=batch_size,
        image_size=image_size,
        shuffle=False,
        seed=random_seed,
    )
    data_flower.load()

    # Model setup
    model = basic_cnn(len(data_flower.class_names), image_size[0], image_size[1])

    optimizer = "Adam"
    loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

    flower_classifier = FlowerClassifier(model=model, loss=loss, optimizer=optimizer)

    # Fitting the model
    mlflow_tags = {
        "description": "Test pipeline",
        "developer": "bryskulov",
        "model_architecture": model.name,
        "train-sample-size": data_flower.get_sample_size()["train"],
        "random_seed": random_seed,
    }
    saved_model_name = f"{model.name}"
    flower_classifier.fit(
        data_flower,
        epochs=epochs,
        mlflow_tags=mlflow_tags,
        saved_model_name=saved_model_name,
        log_models=False,
    )


if __name__ == "__main__":
    main()
